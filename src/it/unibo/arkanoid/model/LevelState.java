package it.unibo.arkanoid.model;
/**
 * Enumeration for the two possible Level states.
 *
 */
public enum LevelState {
    /**
     * Win the game.
     */
    WIN,

    /**
     * Lose the game.
     */
    LOSE
}
